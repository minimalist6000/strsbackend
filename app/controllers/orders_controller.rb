class OrdersController < ApplicationController
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find(params[:id])

    # key = 'key=AIzaSyCkauJJVLMKqj4rSpHAPgj6uj7sjsK2Em4'
    # gcmUrl = 'https://android.googleapis.com/gcm/send'
    # body = {
    #     :registration_ids => ['APA91bGEWeBeyUkCw4QooOY1MuqDWmHR5A8X2iX3vUGfJ35fRBrrEch69a0QyDDrAi8LetItT_1n5ec-vz1dsD-aJFszGjjxhv9v5xjcBAlCtjYwXPjlT2AVxgsI7BwWMjN6b7mUl6vqzSylVWAPlLBcQUyOATbKMOwIqg867gH_EcqRex-aeLs'],
    #     :data => {:message => "Hello Android ... it's Ruby!"}
    # }
    # response = RestClient.post gcmUrl, body.to_json,
    #                            {:content_type => :json, :authorization => key}
    # SuckerPunch.logger.info "Notification sent via GCM"
    # SuckerPunch.logger.info "GCM Response: #{response}"

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/new
  # GET /orders/new.json
  def new
    @order = Order.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id])
  end

  # POST /orders
  # POST /orders.json

  # def create
  #   #booking = params[:booking]
  #   BookingsAsyncJob.new.async.perform("Your booking has been confirmed", nil)
  #   render :text => {:message => 'Information is being processed'}.to_json, :status => :created
  # end

  def create
    @order = Order.new(params[:order])
    @order.status = "Looking for closest Taxi"
    @order.save!
    if @order.pickup_time == ''
      BookingsAsyncJob.new.async.perform(@order, params[:device_id])
      render :text => {:message => 'Submitted information is being processed'}.to_json, :status => :created
    else
      BookingsAsyncJob.get_wait_time(@order.pickup_time, 15)
      BookingsAsyncJob.new.async.perform_deferred(@order)
      m = 'Your taxi will arrive at ' + @order.pickup_time
      render :text => {:message => m}.to_json, :status => :created
    end


    #BookingsAsyncJob.new.async.perform(params[:order], params[:device_id])

    #BookingsAsyncJob.new.async.perform(@order, params[:device_id])
    #BookingsAsyncJob.new.async.perform_deferred(10, @order)


#     @order = Order.new(params[:order])

#     respond_to do |format|
#       if @order.save
#         format.html { render :text => {:message => 'Submitted information is being processed'}.to_json, :status => :created }
#
#         #format.html { redirect_to @order, notice: 'Order was successfully created.' }
# #        format.json { render json: @order, status: :created, location: @order }
#
#         format.json { render :text => {:message => 'Submitted information is being processed'}.to_json, :status => :created }
#
#       else
#         format.html { render action: "new" }
#         format.json { render json: @order.errors, status: :unprocessable_entity }
#       end
#     end
  end



  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find(params[:id])

    respond_to do |format|
      if @order.update_attributes(params[:order])
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to orders_url }
      format.json { head :no_content }
    end
  end


  # PUT /orders/1
  # PUT /orders/1.json
  def accept
    @order = Order.find(params[:id])
    @order.status = "Order accepted"
    @order.save

    render :text => {:message => 'Order was successfully updated.'}.to_json, :status => :created

    message = 'Your taxi will arrive in: ' + @order.duration
    Pusher['bookings'].trigger('async_notification', {:message => message})
    SuckerPunch.logger.info "Notification sent via Pusher"
  end

  def reject
    @order = Order.find(params[:id])
    @order.status = "Order rejected"
    @order.taxi_id = ''
    @order.save

    render :text => {:message => 'Order was successfully updated.'}.to_json, :status => :created

    message = 'No taxis are available'
    Pusher['bookings'].trigger('async_notification', {:message => message})
    SuckerPunch.logger.info "Notification sent via Pusher"
  end
end
