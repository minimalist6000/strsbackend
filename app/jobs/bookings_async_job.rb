class BookingsAsyncJob
  include SuckerPunch::Job

#  def perform(message, location, device_id)

  def perform_deferred(order)
    waitTime = BookingsAsyncJob.get_wait_time(order.pickup_time, 30)
    #waitTime = 5
    after(waitTime) { perform(order, nil) }
  end

  def perform(order, device_id)
    # unless device_id.nil?
    # key = 'key=AIzaSyBnSf2A_7yc4W2sOmZQxIJCi4Ih-DsUK2I'
    # gcmUrl = 'https://android.googleapis.com/gcm/send'
    # body = {
    #   :registration_ids => [device_id],
    #   :data => {:message => "Hello Android ... it's Ruby!"}
    # }
    # response = RestClient.post gcmUrl, body.to_json,
    #               {:content_type => :json, :authorization => key}
    # SuckerPunch.logger.info "Notification sent via GCM"
    # SuckerPunch.logger.info "GCM Response: #{response}"
    # else
    #taxiId = Geolocator.getClosestTaxiIdByLocation(order.start_location)

    taxiId, duration, distance = getClosestTaxiIdByLocation(order.start_location)
    order.customer_name = "John Smith"
    order.customer_phone = "555-555-555"
    order.status = "Waiting for approval"
    order.taxi_id = taxiId
    order.duration = duration
    order.distance = distance
    order.save

    messageToTaxiHome = '{"name": "'+order.customer_name.to_s+'", "address": "'+order.start_location.to_s+'", "phone": "'+order.customer_phone.to_s+'", "order_id" : "'+order.id.to_s+'"}'

    Pusher['taxiHome'].trigger('async_notification',  {:message => messageToTaxiHome})
    SuckerPunch.logger.info "Notification sent via Pusher"

    # end
  end



  def getClosestTaxiIdByLocation(clientLocation)
    closestTaxi = nil
    closestTaxiDistance = 9999999999999999
    closestTaxiDuration = nil

    taxis = Taxi.all
    taxis.each do |taxi|
      url = buildGoogleMapsDirectionUrl(clientLocation, taxi.location)
      distance, duration = getDistanceDataByUrl(url)
      if distance < closestTaxiDistance
        closestTaxi = taxi
        closestTaxiDistance = distance
        closestTaxiDuration = duration
      end
    end

    if !closestTaxi.nil?
      return closestTaxi.id, closestTaxiDuration, closestTaxiDistance
    end
  end

  def buildGoogleMapsDirectionUrl(clientLocation, taxiLocation)
    link  = 'https://maps.googleapis.com/maps/api/directions/json?origin='
    link += trimLocation taxiLocation
    link += '&destination='
    link += trimLocation clientLocation
    link += '&sensor=false&key=AIzaSyDRsDjVUnfJZLZm_cezxSgYsQIulIsQPe4'
  end

  def trimLocation(location)
    if !location.nil?
      return location.gsub(' ', '')
    end
  end


  def getDistanceDataByUrl(url)
    distanceInMeters = nil
    durationText = nil

    response = RestClient.get url
    topObj = JSON.parse response
    routes = topObj["routes"]
    routes.each do |route|
      legs = route["legs"]
      legs.each do |leg|
        distance = leg["distance"]
        distanceInMeters = distance["value"]

        duration = leg["duration"]
        durationText = duration["text"]
      end
    end

    return distanceInMeters, durationText;
  end


  def self.get_wait_time(pickup_time, startPrior)
    now = Time.now.strftime("%H:%M")
    if dt = DateTime.parse(now) rescue false
      from_sec = dt.hour * 3600 + dt.min * 60
    end
    if dt = DateTime.parse(pickup_time) rescue false
      to_sec = dt.hour * 3600 + dt.min * 60
    end
    puts to_sec.to_s

    diff = to_sec - from_sec - startPrior
    puts diff.to_s
    return diff
  end


end

