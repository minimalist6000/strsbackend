class Geolocator

  def self.getClosestTaxiIdByLocation(clientLocation)
    closestTaxi = nil
    closestTaxiDistance = 9999999999999999
    taxis = Taxi.all
    taxis.each do |taxi|
      url = buildGoogleMapsDirectionUrl(clientLocation, taxi.location)
      distance = getDistanceByUrl(url)
      if distance < closestTaxiDistance
        closestTaxi = taxi
        closestTaxiDistance = distance
      end
    end

    if !closestTaxi.nil?
      puts "closestTaxi"
      puts closestTaxiDistance
      closestTaxi.id
    end
  end

  def self.buildGoogleMapsDirectionUrl(clientLocation, taxiLocation)
    link  = 'https://maps.googleapis.com/maps/api/directions/json?origin='
    link += trimLocation taxiLocation
    link += '&destination='
    link += trimLocation clientLocation
    link += '&sensor=false&key=AIzaSyDRsDjVUnfJZLZm_cezxSgYsQIulIsQPe4'
  end

  def self.trimLocation(location)
    if !location.nil?
      return location.gsub(' ', '')
    end
  end


  def self.getDistanceByUrl(url)
    response = RestClient.get url
    topObj = JSON.parse response
    distanceInMeters = nil
    routes = topObj["routes"]
    routes.each do |route|
      legs = route["legs"]
      legs.each do |leg|
        distance = leg["distance"]
        distanceInMeters = distance["value"]

      end
    end

    return distanceInMeters;
  end
end