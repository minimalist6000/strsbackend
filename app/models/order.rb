class Order < ActiveRecord::Base
  attr_accessible :cost, :customer_name, :customer_phone, :end_date, :end_location, :start_date, :start_location, :status, :taxi_id, :distance, :duration, :pickup_time
end
