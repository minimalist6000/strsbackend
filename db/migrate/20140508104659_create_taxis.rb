class CreateTaxis < ActiveRecord::Migration
  def change
    create_table :taxis do |t|
      t.integer :taxi_id
      t.string :location
      t.string :status
      t.string :name

      t.timestamps
    end
  end
end
