class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :order_id
      t.integer :taxi_id
      t.string :start_location
      t.string :end_location
      t.datetime :start_date
      t.datetime :end_date
      t.string :status
      t.string :customer_name
      t.string :customer_phone
      t.float :cost

      t.timestamps
    end
  end
end
