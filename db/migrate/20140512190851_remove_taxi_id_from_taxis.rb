class RemoveTaxiIdFromTaxis < ActiveRecord::Migration
  def up
    remove_column :taxis, :taxi_id
  end

  def down
    add_column :taxis, :taxi_id, :integer
  end
end
