class ChangeOrdersCostFromFloatToDecimal < ActiveRecord::Migration
  def change
    remove_column :orders, :cost
    add_column :orders, :cost, :decimal, :precision => 5, :scale => 2
  end
end
