class AddDurationToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :duration, :string
  end
end
