# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140529073512) do

  create_table "orders", :force => true do |t|
    t.string   "start_location"
    t.string   "end_location"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "status"
    t.string   "customer_name"
    t.string   "customer_phone"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.decimal  "price",          :precision => 5, :scale => 2
    t.integer  "taxi_id"
    t.decimal  "cost",           :precision => 5, :scale => 2
    t.string   "distance"
    t.string   "duration"
    t.string   "pickup_time"
  end

  add_index "orders", ["taxi_id"], :name => "index_orders_on_taxi_id"

  create_table "taxis", :force => true do |t|
    t.string   "location"
    t.string   "status"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
