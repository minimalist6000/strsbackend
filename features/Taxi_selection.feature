Feature: Taxi selection
  As a customer
  So that I can have a taxi booking confirmed
  I want one taxi to be selected

  @javascript
  Scenario: Closest taxi
    Given I have access to STRS web application
    And I am at Liivi 2
    And there are two taxi available, one next to Kaubamaja and the other at Lounakeskus
    When I submit a booking request
    Then I should be notified that my booking request is being processed
    And I should later receive a confirmation with the taxi next to Kaubamaja

  Scenario: Multiple candidate taxis
    Given there are two taxi available, at the same distance with respect to Liivi
    When Customer submits a booking request
    Then I should receive a confirmation with the taxi that has been available the longest
    And I should receive a delay estimate