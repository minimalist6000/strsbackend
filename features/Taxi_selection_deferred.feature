Feature: Taxi selection to specified time
  As a customer
  I want to order a taxi ahead of time (currently it's 08:00)
  So that I could attend a meeting later today (10:15)

  Background:
    Given there are taxis available at 10:00

  @javascript
  Scenario: Successful booking of a taxi
    Given I am on taxi booking web page
    When I enter address, phone, name
    And I enter pickup time 10:15
    And I press Submit
    Then I should get confirmation 'Your taxi will arrive 10:15 (+/- 5 minutes)'
    And I should get additional notification 15 minutes before taxi arrival