Given(/^there are taxis available at (\d+):(\d+)$/) do |arg1, arg2|
  @taxi1 = Taxi.create!(:location => 'Kaubamaja,Tartu,Estonia', :name => 'Taxi A')
end

Given(/^I am on taxi booking web page$/) do
  visit "http://localhost:9090/#/"
end

When(/^I enter address, phone, name$/) do
  fill_in 'address', :with => 'Liivi 2, Tartu, Estonia'
end

When(/^I enter pickup time (\d+):(\d+)$/) do |arg1, arg2|
  time = Time.now + 60
  tStr = time.hour.to_s + ":" + time.min.to_s
  fill_in 'pickup', :with => tStr
end

When(/^I press Submit$/) do
  click_button 'Submit'
end

Then(/^I should get confirmation 'Your taxi will arrive (\d+):(\d+) \(\+\/\- (\d+) minutes\)'$/) do |arg1, arg2, arg3|
  puts "ZZZZZ"
  puts find_by_id('sn').text
  puts find_by_id('sn').text
  expect(find_by_id('sn').text).to match(/Your taxi will arrive at/)
  puts find_by_id('sn').text

  #approve in taxiHome
  # sleep 10
  # visit "http://localhost:9000/#/"
  # click_button 'Ok'
  #
  # #check back to web
  # visit "http://localhost:9000/#/"
  # sleep 2
  # puts "ZZZZ"
  # puts find('div#async-notification').text
  #expect(find('div#async-notification').text).to match(/Your taxi will arrive in/)
end

Then(/^I should get additional notification (\d+) minutes before taxi arrival$/) do |arg1|
  sleep 45
  puts find_by_id('async-notification').text
  expect(find_by_id('async-notification').text).to match(/Your taxi will arrive in/)
  puts find_by_id('async-notification').text
end