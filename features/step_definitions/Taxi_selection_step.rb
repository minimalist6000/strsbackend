Given(/^I have access to STRS web application$/) do
  visit "http://localhost:9090/#/bookings"
end

Given(/^I am at Liivi 2$/) do
  fill_in 'address', :with => 'Liivi 2, Tartu, Estonia'
end

Given(/^there are two taxi available, one next to Kaubamaja and the other at Lounakeskus$/) do
  @taxi1 = Taxi.create!(:location => 'Kaubamaja,Tartu,Estonia', :name => 'Taxi A')
  @taxi2 = Taxi.create!(:location => 'Lounakeskus,Tartu,Estonia', :name => 'Taxi B')
end

When(/^I submit a booking request$/) do
  click_button 'Submit'
end

Then(/^I should be notified that my booking request is being processed$/) do
  puts find_by_id('sn').text
  puts find_by_id('sn').text
  expect(find_by_id('sn').text).to match(/Submitted information is being processed/)
  puts find_by_id('sn').text
end

Then(/^I should later receive a confirmation with the taxi next to Kaubamaja$/) do
  sleep 10
  puts find_by_id('async-notification').text
  expect(find_by_id('async-notification').text).to match(/Your taxi will arrive in/)
  puts find_by_id('async-notification').text
end


Given(/^there are two taxi available, at the same distance with respect to Liivi$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^Customer submits a booking request$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should receive a confirmation with the taxi that has been available the longest$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^I should receive a delay estimate$/) do
  pending # express the regexp above with the code you wish you had
end