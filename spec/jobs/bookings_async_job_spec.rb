require_relative '../../spec/spec_helper'

describe BookingsAsyncJob do

  before {Time.stub(:now) {DateTime.parse("13:00")}}

  describe ".get_wait_time" do
    it "should return seconds between now and given time" do
      c = BookingsAsyncJob.get_wait_time("14:00", 15)
      c.should be(3585)
    end
  end

end
