require 'spec_helper'

describe "orders/index" do
  before(:each) do
    assign(:orders, [
      stub_model(Order,
        :price => "9.99",
        :customer_name => "Customer Name",
        :customer_phone => "Customer Phone",
        :end_location => "End Location",
        :start_location => "Start Location",
        :status => "Status",
        :taxi => nil
      ),
      stub_model(Order,
        :price => "9.99",
        :customer_name => "Customer Name",
        :customer_phone => "Customer Phone",
        :end_location => "End Location",
        :start_location => "Start Location",
        :status => "Status",
        :taxi => nil
      )
    ])
  end

  it "renders a list of orders" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Customer Name".to_s, :count => 2
    assert_select "tr>td", :text => "Customer Phone".to_s, :count => 2
    assert_select "tr>td", :text => "End Location".to_s, :count => 2
    assert_select "tr>td", :text => "Start Location".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
