require 'spec_helper'

describe "orders/new" do
  before(:each) do
    assign(:order, stub_model(Order,
      :price => "9.99",
      :customer_name => "MyString",
      :customer_phone => "MyString",
      :end_location => "MyString",
      :start_location => "MyString",
      :status => "MyString",
      :taxi => nil
    ).as_new_record)
  end

  it "renders new order form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", orders_path, "post" do
      assert_select "input#order_price[name=?]", "order[price]"
      assert_select "input#order_customer_name[name=?]", "order[customer_name]"
      assert_select "input#order_customer_phone[name=?]", "order[customer_phone]"
      assert_select "input#order_end_location[name=?]", "order[end_location]"
      assert_select "input#order_start_location[name=?]", "order[start_location]"
      assert_select "input#order_status[name=?]", "order[status]"
      assert_select "input#order_taxi[name=?]", "order[taxi]"
    end
  end
end
