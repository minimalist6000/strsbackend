require 'spec_helper'

describe "orders/show" do
  before(:each) do
    @order = assign(:order, stub_model(Order,
      :price => "9.99",
      :customer_name => "Customer Name",
      :customer_phone => "Customer Phone",
      :end_location => "End Location",
      :start_location => "Start Location",
      :status => "Status",
      :taxi => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/9.99/)
    rendered.should match(/Customer Name/)
    rendered.should match(/Customer Phone/)
    rendered.should match(/End Location/)
    rendered.should match(/Start Location/)
    rendered.should match(/Status/)
    rendered.should match(//)
  end
end
